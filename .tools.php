<?php
class DataFile {
  private $datafile;
  private $con;

  public function __construct($datafile) {
    $this->datafile = $datafile;

    $host = $_ENV["SQL_HOST"];
    $port = $_ENV["SQL_PORT"];
    $username = $_ENV["SQL_USERNAME"];
    $password = $_ENV["SQL_PASSWORD"];
    $database = $_ENV["SQL_DB"];

    $dsn = "pgsql:host=".$host.";port=".$port.";dbname=".$database.";sslmode=require";

    $this->con = new PDO(
      $dsn,
      $username,
      $password,
      [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]
    );
  }

  public function refresh() {
    $result = $this->con->query("
      SELECT user, current, last_update
      FROM jobs
      WHERE 
        current NOT IN (
          'grant_permissions',
          'background_mail_migration',
          'create_ox_account',
          'pre_migration_warnings'
        )
    ");

    $steps = [
      "schedule_migration" => 0,
      "mail_migration" => 1,
      "forward_and_rule_migration" => 2,
      "start_mailbox_transition" => 3,
      "calendar_migration" => 3,
      "mail_migration_diff_sync" => 3,
      "finish_mailbox_transition" => 4,
      "finish_migration" => 4,
    ];

    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
      $data[] = [
        "user" => $row["user"],
        "step" => $steps[$row["current"]],
        "change" => $row["last_update"]
      ];
    }

    $raw_data = $data == null ? "{}" : json_encode($data);

    file_put_contents($this->datafile, $raw_data);
  }

  public function isStale() {
    if (!file_exists($this->datafile))
      return true;

    return filemtime($this->datafile) + 60 * 5 < time();
  }
}
?>
