<?php
require_once(".tools.php");

$datafile = new DataFile("data.json");

#if ($datafile->isStale())
  $datafile->refresh();
?>
<!doctype html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Mail Migration Status Dashboard</title>
</head>
<style>
body {
  max-width: 800px;
  margin: 0 auto;
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Helvetica, Arial, sans-serif;
  text-align: justify;
}

table {
  width: 100%;
  border-collapse: collapse;
}

td, th {
  margin: 0;
  border: 1px solid #cccccc;
  text-align: left;
  padding: 3px;
}
</style>
<body>

<h1>Mail Migration Status Dashboard</h1>

<p>Users are receiving regular updates by e-mail, this is considered a “nice to have” dashboard to have an idea of where we are. <b>Please rely on the e-mail communications and instructions as primary source of information.</b></p>

<div id="data"></div>

<p>For more details about the migration steps, please visit <a href="https://mailservices.docs.cern.ch/Dovecot/Migration/Overview/" target="_blank">the documentation</a>. In general, for steps 0 and 1, please keep using your old mailbox, for steps 2, 3 and 4 please use your new mailbox.</p>

<h1>FAQ</h1>

<h2>When should I reconfigure my e-mail clients?</h2>
<p><b>Wait for our e-mail telling you to do so.</b> This comes after the “step 1” initial synchronisation.</p>

<h2>How long will my synchronisation last?</h2>
<p>For large and “old” mailboxes it is pretty hard to predict how long it could take. At the moment we have observed that it can take at least a few days for the first sync to complete. This is a function of the number of folders, messages, the total mailbox size and the load on the Exchange server hosting the mailbox at migration time.</p>

<p>We are working on improving the time taken by these operations, mostly focusing on optimising/debugging the Exchange server side (being tracked at MMMMALT-1233).</p>

<h2>Why is the synchronisation so slow?</h2>
<p>This is not a Dovecot issue, but an issue in the Exchange IMAP implementation. Migrating to any other system (Office365, GMail, whatver) would similarly result in issues for large/old mailboxes</p>

<p>In general think about this as an ETL type of exercise from multiple sources (e-mails from Exchange IMAP, calendar/contacts/tasks from exported PSTs, rules and other settings from other sources) to multiple destinations (e-mails to Dovecot IMAP, calendar/contacts/tasks to OX *DAV, rules and other settings to different destinations).</p>

<h2>Is it possible to get a more precise indication of where we are?</h2>
<p>Wherever we can, we have put a rough estimate of the current status of the folders to be migrated as reported by the synchronisation engine.</p>

<h2>Why is my “migrated folder” count going back to a lower value?</h2>
<p>Whenever a sync gets stuck then it may be restarted and the count together with it (think of it as incremental passes, as we fix the problems and “fast forward” back to where the previous pass was stuck). The fact that a sync “gets stuck” is quite normal, not worrying and depends on many factors, especially linked to the load on the Exchange server side.</p>

<h2>I am worried I will have to leave before the migration finished</h2>
<p>Please get in touch with us as we can pause the migration and resume at a later date.</p>

<script>
var url = "https://email-migration.web.cern.ch/data.json";
var stepDescriptions = {
  "0": "migration scheduled",
  "1": "1st email sync",
  "2": "email delivery switched",
  "3": "2nd email sync",
  "4": "migration finalization"
}

fetch(url)
.then(res => res.json())
.then(data => {
  console.log(data);

  var el = document.querySelector("#data");

  content = "<table>";
  content += "<tr>";
  content += "<th>Username</th>";
  content += "<th>Last update</th>";
  content += "<th>Step</th>";
  content += "</tr>";

  for (var i = 0; i < data.length; i++) {
    var userInfo = data[i];
    content += "<tr>";
    content += "<td>"+userInfo.user+"</td>";
    content += "<td>"+userInfo.change+"</td>";
    content += "<td>"+userInfo.step+" ("+stepDescriptions[userInfo.step]+")</td>";
    content += "</tr>";
  }    
  content += "</table>";

  el.innerHTML = content;
})
.catch(err => {
  alert("Could not load migration data.");
  throw err;
});    
</script>

</body>
</html>
